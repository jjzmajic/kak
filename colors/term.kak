face global value green
face global type magenta
face global variable red
face global function magenta
face global module magenta
face global string yellow+i
face global error red
face global keyword blue
face global operator bright-red
face global attribute bright-magenta
face global comment bright-black+i
face global meta red
face global builtin white+b
face global title red
face global header bright-red
face global bold bright-magenta
face global italic magenta
face global mono green
face global block cyan
face global link green
face global bullet green
face global list white

face global Default white,black

face global PrimarySelection bright-white,red
face global PrimaryCursor black,red
face global PrimaryCursorEol black,blue

face global SecondarySelection bright-white,bright-black
face global SecondaryCursor black,bright-red
face global SecondaryCursorEol black,bright-blue

face global MatchingChar black,blue
face global Search blue,black
face global CurrentWord white,blue

# listchars
face global Whitespace bright-black,black+f
# ~ lines at EOB
face global BufferPadding bright-black,black
# must use wrap -marker hl
face global WrapMarker Whitespace

face global LineNumbers bright-black,black
# must use -hl-cursor
face global LineNumberCursor white,bright-black+b
face global LineNumbersWrapped bright-black,black+i

# when item focused in menu
face global MenuForeground blue,black+b
# default bottom menu and autocomplete
face global MenuBackground white,bright-black
# complement in autocomplete like path
face global MenuInfo black,blue
# clippy
face global Information yellow,bright-black
face global Error black,red

# all status line: what we type, but also client@[session]
face global StatusLine white,black
# insert mode, prompt mode
face global StatusLineMode black,green
# message like '1 sel'
face global StatusLineInfo magenta,black
# count
face global StatusLineValue bright-red,black
face global StatusCursor white,blue
# like the word 'select:' when pressing 's'
face global Prompt black,green
