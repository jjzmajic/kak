colorscheme term
set global ui_options ncurses_assistant=none
set global ui_options ncurses_set_title=false # dvtm compat

# plug.kak
nop %sh{
    PLUGINS=${XDG_CONFIG_HOME:-~/.config}/kak/plugins/
    if [ ! -d $PLUGINS ]; then
        mkdir -p $PLUGINS
        git clone https://github.com/andreyorst/plug.kak \
            ~/.config/kak/plugins/plug.kak
    fi
}

source "%val{config}/plugins/plug.kak/rc/plug.kak"
plug "andreyorst/plug.kak" noload
set-option global plug_always_ensure true

# core map
map global normal <space> ,
map global normal , <space>
map -docstring 'shell' global user \' %sh{echo ":terminal ${SHELL}<ret>"}

# plugins
plug "andreyorst/fzf.kak" defer fzf.kak config %{
    map -docstring 'search mode' global user s ':fzf-mode<ret>'
} defer "fzf" %{
    set-option global fzf_highlight_command bat
    set-option global fzf_default_opts %sh{echo $FZF_DEFAULT_OPTS}
}

plug "ul/kak-lsp" defer kak-lsp do %{
    cargo build --release --locked
    cargo install --force --path .
}

plug "andreyorst/smarttab.kak" defer smarttab %{
    set-option global softtabstop 4
}

plug "jjzmajic/kakoune-extra" subset %{
    dvtm.kak
}

plug chambln/kakoune-readline config %{
    map global insert <tab> <c-n>
    map global insert <s-tab> <c-p>
    map global insert <c-p> <up>
    map global insert <c-n> <down>
    hook global WinCreate .* readline-enable
}

plug h-youhei/kakoune-surround config %{
    declare-user-mode surround
    map global surround s ':surround<ret>' -docstring 'surround'
    map global surround c ':change-surround<ret>' -docstring 'change'
    map global surround d ':delete-surround<ret>' -docstring 'delete'
    map global surround t ':select-surrounding-tag<ret>' -docstring 'select tag'
    map global normal S ':enter-user-mode surround<ret>'
}

plug 'delapouite/kakoune-user-modes' %{
    map global user a ':enter-user-mode anchor<ret>'   -docstring 'anchor mode'
    map global user e ':enter-user-mode echo<ret>'     -docstring 'echo mode'
    map global user f ':enter-user-mode format<ret>'   -docstring 'format mode'
    map global user i ':enter-user-mode enter<ret>'    -docstring 'enter mode'
    map global user k ':enter-user-mode keep<ret>'     -docstring 'keep mode'
    map global user l ':enter-user-mode lint<ret>'     -docstring 'lint mode'
    map global user r ':enter-user-mode rotation<ret>' -docstring 'rotation mode'
    map global user t ':enter-user-mode trim<ret>'     -docstring 'trim mode'
}

# git
hook global WinCreate .* %{ git show-diff }
declare-user-mode git
map global git g ':git status<ret>'
map global git s ':git status<ret>'
map global git c ':git commit<ret>'
map global git a ':git add -u .<ret>'
map -docstring 'git mode' global user g ':enter-user-mode git<ret>'

# Local Variables:
# mode: shell-script
# End:
